package com.bt.management.company.demo.services;

import com.bt.management.company.demo.models.Angajat;
import com.bt.management.company.demo.models.Departament;
import com.bt.management.company.demo.models.Functie;
import com.bt.management.company.demo.models.Proiect;
import com.bt.management.company.demo.repositories.AngajatRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

@Service

public class AppService {

    private final AngajatRepository angajatRepository;

    public AppService(AngajatRepository angajatRepository) {
        this.angajatRepository = angajatRepository;
    }

    //1.- Un program/ o metodă/ o funcționalitate care să calculeze salariul unui angajat primit ca
    //parametru

    public Double calculeazaSalariulUnuiAngajat(Angajat angajat, Functie functie) {
        LocalDate dataCurenta = LocalDate.now();
        LocalDateTime dataAngajare = angajat.getDataAngajare();
        long nrAni = ChronoUnit.YEARS.between(dataCurenta, dataAngajare);
        double salariuDeBaza = functie.getSalariuDeBaza();
        Double salariu = salariuDeBaza * nrAni;
        return salariu;

    }


    //2.Un program/ o metodă/ o funcționalitate care primește o listă de angajați și încerarcă
    //atribuirea lor pe un proiect. Se vor afișa angajații din această listă care nu sunt disponibili în
    //perioada derulării proiectului, iar pe cei care sunt disponbili îi va asigna pe proiect.


//3.Un program/ o metodă/ o funcționalitate cu ajutorul căruia se pot lista toți angajații dintr-un
//departament trimis ca parametru.


    public Set<Angajat> afiseazaAngajatiiDinDepartament(Departament departament) {

        String numeDep = departament.getNumeDep();
        Set<Angajat> angajatList = departament.getAngajatSet();
        return angajatList;
    }

    //4.- Un program/ o metodă/ o funcționalitate care să primește ca parametru o entitate de tip
//proiect și care returnează o listă de departamente care reprezintă departamentele implicate
//în acel proiect.

//    public Set<Departament> afiseazaDepartamenteleDePeProiect(Proiect proiect, Departament departament) {
//        String numeProiect = proiect.getNumeProiect();
//
//
//        return ;
//    }


//5.- Un program/ o metodă/ o funcționalitate care să primească ca parametrii un angajat care
//hotărăște să părăsească compania și un înlocuitor al acestuia. Este nevoie ca acest cod să
//realizeze interschimbarea celor doi în toate rolurile pe care le are angajatul care părăsește
//compania (Departamente, Proiecte, superior ierarhic direct)

}
