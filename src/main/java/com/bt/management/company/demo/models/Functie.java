package com.bt.management.company.demo.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Functie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String denumireFunctie;

    @Column
    private Double salariuDeBaza;

    @OneToMany(mappedBy = "numeFunctie")
    private Set<Angajat> angajatSet;


}
