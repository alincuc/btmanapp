package com.bt.management.company.demo.controller;

import com.bt.management.company.demo.models.Angajat;
import com.bt.management.company.demo.repositories.AngajatRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping(value = "/angajat")
public class AngajatController {
    private final AngajatRepository  angajatRepository;

    public AngajatController(AngajatRepository angajatRepository) {
        this.angajatRepository = angajatRepository;
    }

    @GetMapping("/add") //create
    private String creeazaAngajat(Model model) {
        model.addAttribute("angajat", new Angajat());
        return "angajat/add_angajat";
    }

    @GetMapping("/list") //read
    private String angajatList(Model model) {
        Set<Angajat> angajatSet = (Set<Angajat>) angajatRepository.findAll();
        model.addAttribute("angajat", angajatSet);
        return "angajat/angajati";
    }

    @PostMapping("/update") //update
    private String editAngajat(@RequestParam("angajat_id") Long angajatId, Model model) {
        Optional<Angajat> angajatOptional = angajatRepository.findById(angajatId);
        Angajat angajat = angajatOptional.orElseGet(Angajat::new);
        model.addAttribute("angajat", angajat);
        return "angajat/add_angajat";
    }

    @DeleteMapping("/delete") //delete
    private String delete(@RequestParam("angajat_id") Long angajatId) {
        angajatRepository.deleteById(angajatId);
        return "redirect:/angajat/list";
    }



}
