package com.bt.management.company.demo.controller;
import com.bt.management.company.demo.models.Departament;
import com.bt.management.company.demo.repositories.DepartamentRepositoy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping(value = "/departament")
public class DepartamentController {

    private final DepartamentRepositoy departamentRepositoy;

    public DepartamentController(DepartamentRepositoy departamentRepositoy) {
        this.departamentRepositoy = departamentRepositoy;
    }

    @GetMapping("/add") //create
    private String creeazaDepartament(Model model) {
        model.addAttribute("departament", new Departament());
        return "departament/add_departament";
    }

    @GetMapping("/list") //read
    private String departamentList(Model model) {
        Set<Departament> departamentSet = (Set<Departament>) departamentRepositoy.findAll();
        model.addAttribute("departament", departamentSet);
        return "departament/departamente";
    }

    @PostMapping("/update") //update
    private String editDepartament(@RequestParam("departament_id") Long departamentId, Model model) {
        Optional<Departament> departamentOptional = departamentRepositoy.findById(departamentId);
        Departament departament = departamentOptional.orElseGet(Departament::new);
        model.addAttribute("departament", departament);
        return "departament/add_departament";
    }

    @DeleteMapping("/delete") //delete
    private String delete(@RequestParam("departament_id") Long departamentId) {
        departamentRepositoy.deleteById(departamentId);
        return "redirect:/departament/list";
    }

}
