package com.bt.management.company.demo.repositories;

import com.bt.management.company.demo.models.Angajat;
import com.bt.management.company.demo.models.Departament;
import org.springframework.data.repository.CrudRepository;

public interface DepartamentRepositoy  extends CrudRepository<Departament, Long> {
}
