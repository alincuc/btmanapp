package com.bt.management.company.demo.repositories;

import com.bt.management.company.demo.models.Angajat;
import org.springframework.data.repository.CrudRepository;


public interface AngajatRepository extends CrudRepository<Angajat, Long> {


}
