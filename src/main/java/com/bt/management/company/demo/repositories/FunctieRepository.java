package com.bt.management.company.demo.repositories;

import com.bt.management.company.demo.models.Angajat;
import com.bt.management.company.demo.models.Functie;
import org.springframework.data.repository.CrudRepository;

public interface FunctieRepository extends CrudRepository<Functie, Long> {
}
