package com.bt.management.company.demo.models;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Concediu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Date dataStartConcediu;

    @Column
    private Date dataStopConcediu;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "angajat_id")
    private Angajat numeAngajat;
}
