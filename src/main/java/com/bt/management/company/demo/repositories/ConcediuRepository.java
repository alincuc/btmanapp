package com.bt.management.company.demo.repositories;

import com.bt.management.company.demo.models.Angajat;
import com.bt.management.company.demo.models.Concediu;
import org.springframework.data.repository.CrudRepository;

public interface ConcediuRepository extends CrudRepository<Concediu, Long> {
}
