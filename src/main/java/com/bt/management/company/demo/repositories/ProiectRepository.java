package com.bt.management.company.demo.repositories;
import com.bt.management.company.demo.models.Angajat;
import com.bt.management.company.demo.models.Proiect;
import org.springframework.data.repository.CrudRepository;

public interface ProiectRepository extends CrudRepository<Proiect, Long> {
}
