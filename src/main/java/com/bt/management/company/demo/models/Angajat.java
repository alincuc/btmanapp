package com.bt.management.company.demo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Angajat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    @Column
    private String nume;

    @Column
    private String prenume;

    @Column
    private LocalDateTime dataAngajare;

    @Column
    private String superiorIerahic;


    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "functie_id")
    private Functie numeFunctie;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "dep_id")
    private Departament numeDep;

    @OneToMany(mappedBy = "numeAngajat")
    private Set<Concediu> concediuSet;

    @ManyToMany(mappedBy = "angajatSet")
    private Set<Proiect> proiectSet;

}
